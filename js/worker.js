importScripts('rot.js');

const symType = Object.freeze({FLOOR: 0, WALL: 1, ITEM: 2, EXIT: 3})
let width = 0;
let height = 0;
let ncreatures = 0;
let id = 0;
let map = {cells: [], floorCells: [], creatures: [], pathExists: false};

// Fill (over write) all cells that are not the cell type
let floodFill = (x, y, type) => {
  let key = x + y * width;
  if(map.cells[key] !== type) {
    map.cells[key] = type;
    let index = map.floorCells.indexOf(key);
    map.floorCells.splice(index, 1);

    floodFill(x - 1, y, type);
    floodFill(x + 1, y, type);
    floodFill(x, y - 1, type);
    floodFill(x, y + 1, type);
  }
}

// Map generation callback function
let mapCB = (x, y, value) => {
    let key = x + width * y;
    if(value) {
      map.cells[key] = symType.WALL;
    } else {
      map.floorCells.push(key);
      map.cells[key] = symType.FLOOR;
    }
}

// Input callback informs about map structure
let isPassable = (x, y) => {
  const key = x + width * y;
  return (map.cells[key] !== symType.WALL);
}

// Set cell to WALL cell type, removing from floorCells
let setWall = (key) => {
  let index = map.floorCells.indexOf(key);
  if(index > -1) map.floorCells.splice(index, 1);
  map.cells[key] = symType.WALL;
}

// Generate creatures
let generateCreatures = (floorCells) => {
  for(let i = 0; i < ncreatures; i++) {
    let index = Math.floor(ROT.RNG.getUniform() * floorCells.length);
    let key = floorCells.splice(index, 1)[0];
    map.creatures.push(key);
  }
}

// Generate cavern map
let generateCavern = (seed) => {
  seed = seed || 0;
  if(seed !== 0) {
    console.log(`Seed: ${seed}`);
    ROT.RNG.setSeed(seed);
  } else {
    seed = ROT.RNG.getSeed();
    console.log(`System seed: ${seed}`);
  }

  // Loop map creation until fraction of cells is greater than 50% floor cells.
  let fraction = 0.0;
  while(fraction < 0.5) {
    map = new ROT.Map.Cellular(width, height);

    // Cells with 50% probability
    map.randomize(0.5);

    map.cells = new Array(width*height);
    map.floorCells = [];
    map.creatures = [];
    map.pathExists = false;

    // Generate 4 generations
    for(let i = 0; i < 3; i++) {
      map.create(null);
    }
    map.create(mapCB.bind(this));

    // Seal room on the top and bottom
    for(let x = 0; x < width; x++) {
      setWall(x + width * 0);
      setWall(x + width * (height - 1));
    }

    // Seal room on the left and right
    for(let y = 0; y < height; y++) {
      setWall(0 + width * y);
      setWall((width - 1) + width * y);
    }

    // Select a point in floorCells at random
    let index = Math.floor(ROT.RNG.getUniform() * map.floorCells.length);
    let key = map.floorCells.splice(index, 1)[0];
    let cx = parseInt(key % width);
    let cy = parseInt(key / width);

    // Prepare dijkstra path to given reference coordinates
    let dijkstra = new ROT.Path.Dijkstra(cx, cy, isPassable);

    // Compute path to each cell, if path does not exist, fill in disconnected region
    for(let dx = 1; dx < width - 1; dx++) {
      for(let dy = 1; dy < height - 1; dy++) {
        map.pathExists = false;
        dijkstra.compute(dx, dy, function(x, y) {
          map.pathExists = true;
        });
        if(!map.pathExists) {
          floodFill(dx, dy, symType.WALL);
        }
      }
    }

    // Compute the fraction of cells that are floor type compared to the total available number of cells
    fraction = parseFloat(map.floorCells.length)/parseFloat(width*height);
    ROT.RNG.setSeed(seed++);
  }

  // Create exit to maze
  let index = Math.floor(ROT.RNG.getUniform() * map.floorCells.length);
  let key = map.floorCells.splice(index, 1)[0];
  map.cells[key] = symType.EXIT;
  map.exits = {};
  map.exits[key] = 0;

  // Populate list of creatures
  generateCreatures(map.floorCells);

  console.log('Successfully created cavern map');
}

// Web worker onmessage hook
this.onmessage = (event) => {
  id = event.data.id;
  width = event.data.width;
  height = event.data.height;
  ncreatures = event.data.ncreatures;
  console.log(`Worker id: ${id}, width: ${width}, height: ${height}, ncreatures: ${ncreatures}`);
  generateCavern();
  let result = {id: id, map: map};
  postMessage(result);
}