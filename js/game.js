window.onload = () => {
  const nworkers = 8;
  let workers = new Array(nworkers + 1);
  let active = 0;

  let Game = {
    display: null,
    log: null,
    status: null,
    logWidth: 41,
    logHeight: 8,
    width: 41,
    height: 41,
    fontSize: 12,
    symType: Object.freeze({FLOOR: 0, WALL: 1, ITEM: 2, EXIT: 3}),
    maps: new Array(nworkers + 1),
    mapIndex: 0,
    nexits: 8,
    ntreasures: 8,
    ncreatures: 4,
    exits: {},
    mazeExit: -1,
    engine: null,
    scheduler: null,
    player: null,
    creature: null,
    treasure: null,
    keyMap: {},

    // Game initialization
    init: function() {
      let options = {
        width: this.width,
        height: this.height,
        fontSize: this.fontSize,
        forceSquareRatio: true
      }

      // Define numpad key-mapping
      this.keyMap[38] = 0;
      this.keyMap[33] = 1;
      this.keyMap[39] = 2;
      this.keyMap[34] = 3;
      this.keyMap[40] = 4;
      this.keyMap[35] = 5;
      this.keyMap[37] = 6;
      this.keyMap[36] = 7;

      // Start web workers
      this._initWebWorker();

      options.width = this.width;
      options.height = 1;
      this.status = new ROT.Display(options);
      document.body.appendChild(this.status.getContainer());

      options.width = this.width;
      options.height = this.height;
      this.display = new ROT.Display(options);
      document.body.appendChild(this.display.getContainer());

      options.width = this.logWidth;
      options.height = this.logHeight;
      this.log = new ROT.Display(options);
      this.log.messages = [];
      document.body.appendChild(this.log.getContainer());

      let canvases = document.getElementsByTagName('canvas');
      for(let i = 0; i < canvases.length; i++) {
        let canvas = canvases[i];
        canvas.id = `canvas_0${i}`;
        canvas.style.display = 'block';
      }

      let seed = 1442796044743;
      this._generateMazeMap(seed);

      this.scheduler = new ROT.Scheduler.Speed();
      this.scheduler.add(this.player, true);
      this.scheduler.add(this.minotaur, true);

      this.engine = new ROT.Engine(this.scheduler);
      this.engine.start();

      this._initLog();

      this.statusWrite(this.player);
    },

    // Initialize web workers
    _initWebWorker: function() {
      if (window.Worker) {
        for(let i = 1; i <= nworkers; i++) {
          let worker = new Worker('js/worker.js');
          worker.onmessage = function(event) {
            console.log(`Host: Message received from worker #${event.data.id}`);
            Game.maps[event.data.id] = event.data.map;

            this.terminate();
            active--;
            if(active === 0) {
              workers = new Array(nworkers + 1);
            }
          };
          worker.postMessage({id: i, width: Game.width, height: Game.height, ncreatures: Game.ncreatures});
          workers[i] = worker;
          active++;
        }
      } else {
        alert('Sorry! No Web Worker support');
      }
    },

    // Initialize log message
    _initLog: function() {
      // Create border for top and bottom of message log
      for(let x = 0; x < this.logWidth; x++) {
        this.log.drawText(x, 0, '#');
        this.log.drawText(x, this.logHeight - 1, '#');
      }

      // Create border for left and right of message log
      for(let y = 0; y < this.logHeight; y++) {
        this.log.drawText(0, y, '#');
        this.log.drawText(this.logWidth - 1, y, '#');
      }

      // Title for message log
      this.log.drawText(2, 0, 'Log');
    },

    // Write to the message log
    logWrite: function(msg) {
      // If the log is full, remove first entry
      if(this.log.messages.length === this.logHeight - 2) {
        this.log.messages.splice(0, 1);
      }

      // Push new message into array
      this.log.messages.push(msg);

      // Display messages from newest to oldest
      let y = 1, fraction = 0.0;
      for(let i = this.log.messages.length - 1; i >= 0; i--) {
        let colour = ROT.Color.toHex(ROT.Color.interpolate([204, 204, 204], [0, 0, 0], fraction));
        this.log.drawText(2, y++, '%c{' + colour + '}' + this.log.messages[i]);
        fraction += 0.15
      }
    },

    statusWrite: function(data) {
      // Clear status canvas element
      for(let i = 0; i < this.width; i++) {
        this.status.drawText(i, 0, ' ');
      }

      let statusStr = 'HP: %c{#0f0}' + data.hp + '/' + data.hpmax + '%c{}';
      statusStr += ' STR: %c{#0f0}' + data.str + '%c{}';
      statusStr += ' DEX: %c{#0f0}' + data.dex + '%c{}';
      statusStr += ' CON: %c{#0f0}' + data.con + '%c{}'; 
      statusStr += ' XP: %c{#0f0}' + data.xp + '%c{}';
      this.status.drawText(1, 0, statusStr);
    },

    // Map generation callback function
    _mapCB: function(x, y, value) {
      // Load current map
      const currentMap = Game.maps[Game.mapIndex];

      let key = x + y * Game.width;
      if(value) {
        currentMap.cells[key] = Game.symType.WALL;
      } else {
        currentMap.floorCells.push(key);
        currentMap.cells[key] = Game.symType.FLOOR;
      }
    },

    // Generate maze map
    _generateMazeMap: function() {
      let maze = new ROT.Map.IceyMaze(this.width, this.height, 0);
      maze.cells = new Array(this.width * this.height);
      maze.floorCells = [];

      // Store maze at mapIndex = 0
      this.maps[this.mapIndex] = maze;

      // Create maze with callback function
      maze.create(Game._mapCB.bind(this));
      console.log('Successfully created maze map');

      // Create treasure chests
      //this._generateTreasures(maze.floorCells);

      // Create exits
      this._generateExits(maze.floorCells);

      // Draw map
      this._drawMap();
      
      // Create and draw the player and a creature
      this.player = this._createEntity(Player, {}, maze.floorCells);
      this.minotaur = this._createEntity(Creature, {}, maze.floorCells);
    },

    // Generate treasures
    _generateTreasures: function(floorCells) {
      // Load current map
      const currentMap = this.maps[this.mapIndex];

      let chests = [];
      for(let i = 0; i < this.ntreasures; i++) {
        let index = Math.floor(ROT.RNG.getUniform() * floorCells.length);
        let key = floorCells.splice(index, 1)[0];
        currentMap.cells[key] = this.symType.ITEM;
        chests.push(key);
      }

      // Put the treasure in a random chest
      let index = Math.floor(ROT.RNG.getUniform() * chests.length);
      let randomKey = chests[index];
      this.treasure = randomKey;
    },

    // Generate exits
    _generateExits: function(floorCells) {
      // Load current map
      const currentMap = this.maps[this.mapIndex];

      this.exits = {};
      for(let i = 1; i <= this.nexits; i++) {
        let index = Math.floor(ROT.RNG.getUniform() * floorCells.length);
        let key = floorCells.splice(index, 1)[0];
        currentMap.cells[key] = this.symType.EXIT;
        this.exits[key] = i;
      }
    },

    // Create entity
    _createEntity: function(entity, options = {}, floorCells = []) {
      let index = Math.floor(ROT.RNG.getUniform() * floorCells.length);
      let key = floorCells.splice(index, 1)[0];
      let x = parseInt(key % this.width);
      let y = parseInt(key / this.width);
      return new entity(x, y, options);
    },

    // Draw a tile
    _drawTile: function(key) {
      // Load current map
      const currentMap = this.maps[this.mapIndex];

      let x = parseInt(key % Game.width);
      let y = parseInt(key / Game.width);
      switch (currentMap.cells[key]) {
        case this.symType.FLOOR: {
          this.display.draw(x, y, ' ', '#000000');
          break;
        }
        case this.symType.WALL: {
          this.display.draw(x, y, '#', '#777777');
          break;
        }
        case this.symType.ITEM: {
          // Brown: #996633
          this.display.draw(x, y, '?', '#0000ff');
          break;
        }
        case this.symType.EXIT: {
          this.display.draw(x, y, '>', '#996633');
          break;
        }
        default: {
          console.log(`Unrecognized option: ${currentMap.cells[key]}`);
          break;
        }
      }
    },

    // Draw the map
    _drawMap: function() {
      // Load current map
      const currentMap = this.maps[this.mapIndex];

      for(let key in currentMap.cells) {
        this._drawTile(key);
      }
    }
  };

  // The player constructor
  class Player {
    constructor(x, y, options = {}) {
      // ATK = (STR + DEX + CON)/3
      // DEF = (STR + DEX + CON)/3 - 2
      // HP = 5*(STR + CON)
      // STUNNED if ATK - DEF >= HP/5

      // Default and derived parameters
      const defaults = {
        sym: '@',
        colour: 'yellow',
        str: 3,
        dex: 3,
        con: 3
      };
      const p = Object.assign({}, defaults, options);

      // Base attributes
      this.str = Math.abs(p.str);
      this.dex = Math.abs(p.dex);
      this.con = Math.abs(p.con);
      this.xp = 0;

      // Derived attributes
      this.hpmax = 5*(this.str + this.con);
      this.hp = this.hpmax;
      this.atk = Math.max(Math.floor((this.str + this.dex + this.con)/3), 1);
      this.def = Math.max(Math.floor((this.str + this.dex + this.con)/3) - 2, 0);

      // Player symbol and colour
      this.sym = p.sym;
      this.colour = p.colour;

      // Position
      this.x = x;
      this.y = y;

      // Draw player
      this._draw();
    }

    getSpeed() {
      return this.dex;
    }

    act() {
      Game.engine.lock();
      window.addEventListener('keydown', this);
    }

    _performAction() {
      // Load current map
      let currentMap = Game.maps[Game.mapIndex];

      const key = this.x + this.y * Game.width;
      const cellType = currentMap.cells[key];
      switch(cellType) {
        case Game.symType.ITEM: {
          if(key === Game.treasure) {
            Game.logWrite('You found the treasure! Thanks for playing.');
            Game.engine.lock();
            window.removeEventListener('keydown', this);
          } else {
            Game.logWrite('This box is empty');
          }
          break;
        }
        case Game.symType.EXIT: {
          if(Game.mapIndex === 0) {
            Game.mazeExit = key;
            Game.mapIndex = Game.exits[key];
          } else {
            Game.mapIndex = 0;
          }

          // Load map at updated mapIndex
          currentMap = Game.maps[Game.mapIndex];
          if(Game.mapIndex > 0) {
            Game.logWrite('You enter a cavern.');
          } else {
            Game.logWrite('You return to the maze.');
          }

          // Clear map canvas element
          const canvas = document.getElementById('canvas_01');
          const context = canvas.getContext('2d');
          context.clearRect(0, 0, canvas.width, canvas.height);

          // Draw map
          Game._drawMap();

          // Update player position to cavern/maze exit
          let exitKey = null;
          if(Game.mapIndex === 0) {
            exitKey = Game.mazeExit;
          } else {
            // If we are in the maze, determine location in cavern of exit
            for(const key in currentMap.exits) {
              if(parseInt(currentMap.exits[key]) === 0) {
                exitKey = parseInt(key);
              }
            }
          }

          this.x = parseInt(exitKey % Game.width);
          this.y = parseInt(exitKey / Game.width);     

          if(Game.mapIndex > 0) {
            // Remove all actors except the player
            Game.scheduler.clear();
            Game.scheduler.add(Game.player, true);

            // Create creatures the first time loading cavern
            for (let i = 0; i < Game.ncreatures; i++) {
              if(currentMap.creatures[i] instanceof Creature === false) {
                const key = currentMap.creatures[i];
                const x = parseInt(key % Game.width);
                const y = parseInt(key / Game.width); 
                const creature = new Creature(x, y, {sym: 'm', colour: 'green', str: 1, dex: 1, con: 1});
                currentMap.creatures[i] = creature;
              }
              Game.scheduler.add(currentMap.creatures[i], true);
            }

            // Draw Player
            this._draw();

            // Draw creatures
            for (let i = 0; i < Game.ncreatures; i++) {
              currentMap.creatures[i]._draw();
            }
          } else {
            // Remove all actors except the player
            Game.scheduler.clear();
            Game.scheduler.add(Game.player, true);
            Game.scheduler.add(Game.minotaur, true);

            // Draw Player
            this._draw();

            // Draw Minotaur
            Game.minotaur._draw();
          }
          break;
        }
        default: {
          break;
        }
      }
    }

    handleEvent(event) {
      // Load current map
      const currentMap = Game.maps[Game.mapIndex];

      let code = event.keyCode;
      if(code === 13 || code === 32) {
        this._performAction();
        return;
      }

      // one of numpad directions?
      if(!(code in Game.keyMap)) {
        return;
      }

      // Is the desired direction walkable
      let dir = ROT.DIRS[8][Game.keyMap[code]];
      let newX = this.x + dir[0];
      let newY = this.y + dir[1];
      let newKey = newX + newY * Game.width;

      if(currentMap.cells[newKey] === Game.symType.WALL) {
        window.removeEventListener('keydown', this);
        Game.engine.unlock();
        return;
      } else {
        if(Game.mapIndex === 0) {
          const dx = this.x - Game.minotaur.x;
          const dy = this.y - Game.minotaur.y;
          const d2 = dx*dx + dy*dy;
          if(d2 <= 1) {
            // Attack!
            Game.minotaur.hp -= Math.max(this.atk - Game.minotaur.def, 0);
            this._draw();
            window.removeEventListener('keydown', this);
            Game.engine.unlock();
            return;
          }
        } else {
          //return;
        }
      }

      let oldKey = this.x + this.y * Game.width;
      Game._drawTile(oldKey);

      this.x = newX;
      this.y = newY;
      this._draw();
      window.removeEventListener('keydown', this);
      Game.engine.unlock();
    }

    _draw() {
      Game.statusWrite(this);
      if(this.hp > 0) {
        Game.display.draw(this.x, this.y, this.sym, this.colour);
      } else {
        Game.display.draw(this.x, this.y, '%', 'white');
        alert('Game Over. You were killed by the Minotaur!');
        Game.logWrite('You lost, better luck next time.');
        Game.engine.lock();
      }
    }
  };

  class Creature {
    constructor(x, y, options = {}) {
      // ATK = (STR + DEX + CON)/3
      // DEF = (STR + DEX + CON)/3 - 2
      // HP = 5*(STR + CON)
      // STUNNED if ATK - DEF >= HP/5

      // Default and derived parameters
      const defaults = {
        sym: 'M',
        colour: 'red',
        str: 5,
        dex: 2,
        con: 5
      };
      const p = Object.assign({}, defaults, options);

      // Base attributes
      this.str = Math.abs(p.str);
      this.dex = Math.abs(p.dex);
      this.con = Math.abs(p.con);
      this.xp = 0;

      // Derived attributes
      this.hpmax = 5*(this.str + this.con);
      this.hp = this.hpmax;
      this.atk = Math.max(Math.floor((this.str + this.dex + this.con)/3), 1);
      this.def = Math.max(Math.floor((this.str + this.dex + this.con)/3) - 2, 0);

      // Creature symbol and colour
      this.sym = p.sym;
      this.colour = p.colour;

      // Position
      this.x = x;
      this.y = y;

      // Draw creature
      this._draw();
    }

    getSpeed() {
      return this.dex;
    }

    act() {
      let px = Game.player.x;
      let py = Game.player.y;
      let passableCallback = function(x, y) {
        // Load current map
        const currentMap = Game.maps[Game.mapIndex];

        let key = x + y * Game.width;
        return (currentMap.cells[key] !== Game.symType.WALL);
      }
      let astar = new ROT.Path.AStar(px, py, passableCallback, {topology: 4});

      let path = [];
      let pathCallback = function(x, y) {
        path.push([x, y]);
      }
      astar.compute(this.x, this.y, pathCallback);

      // Check if creature is able to capture the player
      const dx = this.x - px;
      const dy = this.y - py;
      const d2 = dx*dx + dy*dy;

      path.shift(); // Remove the Minotaur's position

      if (d2 <= 1) {
        // Attack!
        Game.player.hp -= Math.max(this.atk - Game.player.def, 0);
        //window.removeEventListener('keydown', this);
        //Game.engine.unlock();
        return;
      } else if (path.length > 1) {
        let newX = path[0][0];
        let newY = path[0][1];

        let oldKey = this.x + this.y * Game.width;
        Game._drawTile(oldKey);

        this.x = newX;
        this.y = newY;
        this._draw(); 

        // if (d2 <= 1) {
        //   alert('Game Over. You were captured by the Minotaur!');
        //   Game.logWrite('You lost, better luck next time.');
        //   Game.engine.lock();
        // }
      } else {
        // Attack!
        Game.player.hp -= Math.max(this.atk - Game.player.def, 0);
        //window.removeEventListener('keydown', this);
        //Game.engine.unlock();
        return;
      }
    }

    _draw() {
      Game.display.draw(this.x, this.y, this.sym, this.colour);
    }
  };

  Game.init();
};
